package com.amg.petmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PetItem {
    private Long petId;

    private Long studentId;

    private String petType;

    private String petName;

    private LocalDate petBirthday;

    private String studentName;

    private String studentPhone;

    private LocalDate studentBirthday;




}
