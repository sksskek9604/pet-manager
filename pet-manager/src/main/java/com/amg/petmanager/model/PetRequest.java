package com.amg.petmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PetRequest {
    private String petType;
    private String petName;
    private LocalDate petBirthday;



}
