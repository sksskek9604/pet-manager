package com.amg.petmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StudentRequest {
    private String name;

    private String phone;

    private LocalDate birthday;

}
