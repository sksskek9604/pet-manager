package com.amg.petmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Pet {

    //1. student entity를 알아야하니까, 자료형이 student임, 변수명도 그대로 자동완성 해도 됨.
    //2. 빨간줄이 떴다.
    //3. 김철수가 고양이 2마리키우고, 영길이가 강아지 4마리나 키우고 있음(선생님꺼)
    //4. 1:N의 관계에서 N은 무조건 1개가 있어야할까? X 0부터 시작해도 상관이 없음 N은 자연수, 정수
    //5. pet 테이블에서 작업하고 있으니, 동창테이블은 1이지만 pet 테이블이 많은 쪽, N에 해당하는 테이블이라고 볼 수 있다.
    //6. 내 중심(student 테이블)으로 보니까, 주석은 @ManytoOne이 된다.
    //7. 필요할 때만 데이터를 끌고오도록 fetch 타입은 lazy로 한다.
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "studentId", nullable = false)
    private Student student;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String petType;

    @Column(nullable = false, length = 50)
    private String petName;

    @Column(nullable = false)
    private LocalDate petBirthday;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;


}
