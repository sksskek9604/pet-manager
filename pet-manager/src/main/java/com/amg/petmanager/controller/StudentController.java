package com.amg.petmanager.controller;

import com.amg.petmanager.model.StudentRequest;
import com.amg.petmanager.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    //1. 안내데스크에 앉혀놓을 사람을 만들자.
    //2. 고객한테 말을 해야한다는 점
    //3. 박스에 한번에 담아서 줘야한다. 왜? 15개 들어있는 쿠키 환불할때 12개 먹고 3개 남기고 해달라하면 안되죠..?
    //4. 그러니까, student에 있던거, 즉 다 채워서 줘야합니다. = 어떻게 채워올지, 어떤 박스인지가 model
    //5. 고객이 주변 바디로 받아올거죠? @RequestBody
    //6. 스튜턴트서비스야 받아줘. 스튜던트 안에 있는 형태들 풀어서!
    @PostMapping("/new")
    public String setStudent(@RequestBody StudentRequest request){
        studentService.setStudent(request.getName(), request.getPhone(), request.getBirthday());
        return "OK";

    }
}
