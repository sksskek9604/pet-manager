package com.amg.petmanager.controller;

import com.amg.petmanager.service.StudentManageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student-manage")
public class StudentMangeController {
    private final StudentManageService studentManageService;

    @DeleteMapping("/{id}")
    public String delStudent(@PathVariable long id) {
        studentManageService.delStudent(id);

        return "OK";
    }
}
