package com.amg.petmanager.controller;

import com.amg.petmanager.entity.Student;
import com.amg.petmanager.model.PetItem;
import com.amg.petmanager.model.PetRequest;
import com.amg.petmanager.service.PetService;
import com.amg.petmanager.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pet")
public class PetController {

    private final PetService petService;
    private final StudentService studentService;
    // 1. 하지만 고객은 Student student 모양을 줄 수 없다.
    // 2. 왔다갔다할 수 있는건 레포지터리만 가능하고, 스튜턴트레포지터리만 스튜턴트를 가지고 올 수 있다.
    // 그래서 스튜턴트 서비스에게 협조를 받고, 서비스가 레포지터리한테 시키게 해준다.
    // 3. 애초, 각각의 엔티티에 맞게 레포지터리와 서비스, 컨트롤러가 있으니 직접 호출을 하게 되면 효율이 떨어진다.
    // 서비스가 각각에 맞게 따로따로 움직여야 되는데, 한번에 쓰게되면 한번에 두개가 호출이 되니까.
    // 4. 고객한테 결국, 'Student student'를 받으려면 어떻게 받아야할까?

    // 1. 1번 회원에, 이 동물 좀(=박스 = model) 등록해줘
    // 2. 박스모양이어야 하니까 바디가 필요. @RequestBody
    // 3. SQL에서 했던 것처럼, id만 알면 id기준으로 entity 가지고 올 수 있으니 studentservice한테 부탁하면
    // studentRepository를 알고 있으니 주고받으면 되는 것.
    @PostMapping("/student/{studentId}")
    public String setPet(@PathVariable long studentId, @RequestBody PetRequest request) { // 1번 회원에
        Student student = studentService.getStudent(studentId);
        petService.setPet(student, request.getPetType(), request.getPetName(), request.getPetBirthday());

        return "OK";
    }

    @GetMapping("/all")
    public List<PetItem> getPets() {
        return petService.getPets();
    }

    @PutMapping("/{id}")
    public String putPet(@PathVariable long id, @RequestBody PetRequest petRequest) {
        petService.putPets(id, petRequest);
        return "OK";
    }

    @PutMapping("/pet-id/{petId}/student-id/{studentId}") //pet-id는 이거{}이고, student-id는 이거{}이다.
    public String putPetOwner(@PathVariable long petId, @PathVariable long studentId) {
        Student student = studentService.getStudent(studentId);
        petService.putPetOwner(petId, student);

        return "OK";
    }

    @DeleteMapping("/{petId}")
    public String delPet(@PathVariable long petId) {
        petService.delPet(petId);

        return "OK";
    }
}
