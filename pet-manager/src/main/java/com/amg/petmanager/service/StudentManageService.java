package com.amg.petmanager.service;

import com.amg.petmanager.entity.Pet;
import com.amg.petmanager.repository.PetRepository;
import com.amg.petmanager.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
//두개 레포지터리를 다 왔다갔다 할거다. 그럴려면 두명 다 알아야함.
//그래서 따로 만든 것
public class StudentManageService {
    private final PetRepository petRepository;

    private final StudentRepository studentRepository;

    //1. 명확한 구분자 필요 = 동창생 시퀀스만 알면 반려동물도 포함되어있으니
    //2. 지울 때 동창생 시퀀스 필요함.
    //3. 만약에 동창생 시퀀스부터 지우면, 동창생 시퀀스가 포함되어있는 반려동물 테이블에서
    //혼란이 나타남. 그렇기 때문에 우선은 반려동물 테이블에서 지워야하는데
    //그러려면 지우려고하는 동창생 시퀀스 번호를 '불러온다'
    public void delStudent(long studentId) {
    List<Pet> pets = petRepository.findAllByStudent_Id(studentId);

    for ( Pet pet : pets) {
        petRepository.deleteById(pet.getId());
    }

       studentRepository.deleteById(studentId);
    }
}
