package com.amg.petmanager.service;

import com.amg.petmanager.entity.Pet;
import com.amg.petmanager.entity.Student;
import com.amg.petmanager.model.PetItem;
import com.amg.petmanager.model.PetRequest;
import com.amg.petmanager.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetService {
    // pet창고만 왔다갔다하면 되죠? 누구랑 일해요?
    private final PetRepository petRepository;
    // 이제 등록할건데, 마찬가지로 자동생성 제외하고 가지고올 것들을 가지고 온다.
    // 스튜던트시퀀스도 받아야하고, 펫타입, 펫네임, 펫생일도 받아야함
    // 그러려면 스튜턴드라는 엔티티가 필요하지 않은가?
    // setPet이 등록해주려는 사람이죠. 아래 4가지 받기로 했다는거죠.
    // 빈박스, 빈 객체 세팅하고 창고에 넣으면 되겠죠?
    // 앞이랑 다르게 추가된 것은 'Student student'

    public void setPet(Student student, String petType, String petName, LocalDate petBirthday) {

        Pet pet = new Pet();         //저장하자. 그러려면 빈 객체를 먼저 만든다.
        pet.setStudent(student);
        pet.setPetType(petType);
        pet.setPetName(petName);
        pet.setPetBirthday(petBirthday);
        pet.setDateCreate(LocalDateTime.now());
        pet.setDateUpdate(LocalDateTime.now());

        //레포지터리야 창고에 좀 갖다놔줘(객체에)
        petRepository.save(pet);
    }
    //1. 결과값을 담을 변수를 만든다. 결과값을 list로 받아야하니까
    //2. list는 늘 쓸 수 있게 초기화하라.
    //3. 받아온다. 그다음 레포지터리에게 다 가지고오라고 한다.
    //4. 이제 재가공만 하면 된다. 재가공을 하려면 반복한다.
    //5. 재가공을 하고 result에 넣으려면 Petitem
    public List<PetItem> getPets() {
        List<PetItem> result = new LinkedList<>();

        List<Pet> pets = petRepository.findAll();
        for (Pet pet : pets) {
            PetItem addItem = new PetItem();
            addItem.setPetId(pet.getId());
            addItem.setStudentId(pet.getStudent(). getId()); // 펫에 id가 이미 들어가있으니까.
            addItem.setPetType(pet.getPetType());
            addItem.setPetName(pet.getPetName());
            addItem.setPetBirthday(pet.getPetBirthday());
            addItem.setStudentName(pet.getStudent().getName());
            addItem.setStudentPhone(pet.getStudent().getPhone());
            addItem.setStudentBirthday(pet.getStudent().getBirthday());

            result.add(addItem);

        }

        return result;
    }
    public void putPets(long id, PetRequest petRequest) {
        Pet pets = petRepository.findById(id).orElseThrow();
        pets.setPetType(petRequest.getPetType());
        pets.setPetName(petRequest.getPetName());
        pets.setPetBirthday(petRequest.getPetBirthday());
        pets.setDateUpdate(LocalDateTime.now());

        petRepository.save(pets);
    }

    public void putPetOwner(long id, Student student) {
        Pet pet = petRepository.findById(id).orElseThrow();
        pet.setStudent(student);

        petRepository.save(pet);
    }

    public void delPet(long id) {
        petRepository.deleteById(id);
    }
}
