package com.amg.petmanager.service;

import com.amg.petmanager.entity.Student;
import com.amg.petmanager.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor //final의 짝꿍 생성자
public class StudentService {
    private final StudentRepository studentRepository; //서비스가 같이 일할 사람 // 나 창고에서 갖다줬어~

    public Student getStudent(long id) {
        return studentRepository.findById(id).orElseThrow();
    }

    public void setStudent(String name, String phone, LocalDate birthday) { //동창생 등록하려면 이름,번호,생일 순서로 알려줘
        //값을 제공할 때
        //저장하자. 그러려면 객체를 먼저 만든다.
        Student student = new Student();
        //받은걸 넣자.
        student.setName(name); //이름
        student.setPhone(phone); //전화번호
        student.setBirthday(birthday); //생일
        student.setDateCreate(LocalDateTime.now()); //등록시간
        student.setDateUpdate(LocalDateTime.now()); //수정시간

        //레포지터리야 창고에 좀 갖다놔줘(객체에)
        studentRepository.save(student);
    }

}
