package com.amg.petmanager.repository;

import com.amg.petmanager.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetRepository extends JpaRepository<Pet,Long> {
    List<Pet> findAllByStudent_Id(long id);
}
